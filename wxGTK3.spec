Name:           wxGTK3
Version:        3.2.6
Release:        2
Summary:        C++ Library for Cross-Platform Development

License:        GPL-2.0-or-later or LGPL-2.1-only
URL:            https://www.wxwidgets.org/
Source0:        https://github.com/wxWidgets/wxWidgets/releases/download/v%{version}/wxWidgets-%{version}.tar.bz2
Source1:        wx-config
Patch0001:      wxGTK3-3.1.6-abicheck.patch
Patch0002:      add-pie-compile-option.patch

BuildRequires: gcc-c++
BuildRequires: pkgconfig(cairo) >= 1.2
BuildRequires: pkgconfig(egl) >= 1.5
BuildRequires: pkgconfig(fontconfig) >= 2.8.0
BuildRequires: pkgconfig(gstreamer-1.0)
BuildRequires: pkgconfig(gstreamer-player-1.0) >= 1.7.2.1
BuildRequires: pkgconfig(gstreamer-video-1.0)
BuildRequires: pkgconfig(gtk+-2.0) >= 2.6.0
BuildRequires: pkgconfig(gtk+-3.0)
BuildRequires: pkgconfig(gtk+-unix-print-3.0)
BuildRequires: pkgconfig(libcurl)
BuildRequires: pkgconfig(libnotify) >= 0.7
BuildRequires: pkgconfig(libpcre2-32)
BuildRequires: pkgconfig(libpng) >= 0.9
BuildRequires: pkgconfig(libsecret-1)
BuildRequires: pkgconfig(libtiff-4)
BuildRequires: pkgconfig(pangoft2) >= 1.38.0
BuildRequires: pkgconfig(pangoxft)
BuildRequires: pkgconfig(sdl2) >= 2.0.0
BuildRequires: pkgconfig(wayland-egl)
BuildRequires: pkgconfig(webkit2gtk-4.1)
BuildRequires: pkgconfig(xkbcommon)
BuildRequires: pkgconfig(xtst)
BuildRequires: cppunit-devel
BuildRequires: expat-devel
BuildRequires: libGLU-devel
BuildRequires: libjpeg-devel
BuildRequires: libmspack-devel
BuildRequires: libSM-devel
BuildRequires: zlib-devel
BuildRequires: doxygen
BuildRequires: gettext

Requires:       wxBase3 = %{version}-%{release} wxGTK3-i18n = %{version}-%{release}
Provides:       wxWidgets = %{version}-%{release} bundled(scintilla) = 3.2.1

Provides:       wxGTK3-gl = %{version}-%{release} wxGTK3-media = %{version}-%{release} wxGTK3-webview = %{version}-%{release}
Obsoletes:      wxGTK3-gl < %{version}-%{release} wxGTK3-media < %{version}-%{release} wxGTK3-webview < %{version}-%{release}

%description
wxWidgets is a C++ library for cross-platform GUI.
With wxWidgets, you can create applications for different GUIs (GTK+,
Motif, MS Windows, MacOS X, Windows CE, GPE) from the same source code.
This package contains all library of GTK3-backed wxWidgets.

%package -n     compat-wxGTK3-gtk2
Summary:        C++ Library for Cross-Platform Development of GTK2-backed wxWidgets
Requires:       wxBase3 = %{version}-%{release} wxGTK3-i18n = %{version}-%{release}
Provides:       compat-wxWidgets-gtk2 = %{version}-%{release} bundled(scintilla) = 3.2.1

Provides:       compat-wxGTK3-gtk2-gl = %{version}-%{release} compat-wxGTK3-gtk2-media = %{version}-%{release}
Obsoletes:      compat-wxGTK3-gtk2-gl < %{version}-%{release} compat-wxGTK3-gtk2-media < %{version}-%{release}

%description -n compat-wxGTK3-gtk2
wxWidgets is a C++ library for cross-platform GUI.
With wxWidgets, you can create applications for different GUIs (GTK+,
Motif, MS Windows, MacOS X, Windows CE, GPE) from the same source code.
This package contains all library of GTK2-backed wxWidgets.

%package -n     wxBase3-devel
Summary:        Development files for GTK2-backed and GTK3-backed wxWidgets
Requires:       wxBase3 = %{version}-%{release}
Requires(post): chkconfig
Requires(postun): chkconfig

%description -n wxBase3-devel
This package contains all files needed for developing with GTK2-backed and GTK3-backed wxWidgets.

%package        devel
Summary:        Development files for GTK3-backed wxWidgets
Requires:       wxGTK3 = %{version}-%{release} wxGTK3-gl = %{version}-%{release}
Requires:       wxGTK3-media = %{version}-%{release} wxGTK3-webview = %{version}-%{release}
Requires:       wxBase3  = %{version}-%{release} wxBase3-devel = %{version}-%{release}
Requires:       gtk3-devel libGLU-devel
Provides:       wxWidgets-devel = %{version}-%{release}

%description    devel
This package contains all files needed for developing with GTK3-backed wxWidgets.

%package -n     compat-wxGTK3-gtk2-devel
Summary:        Development files for GTK2-backed wxWidgets
Requires:       compat-wxGTK3-gtk2 = %{version}-%{release} compat-wxGTK3-gtk2-gl = %{version}-%{release}
Requires:       compat-wxGTK3-gtk2-media = %{version}-%{release}
Requires:       wxBase3 = %{version}-%{release} wxBase3-devel = %{version}-%{release}
Requires:       gtk2-devel libGLU-devel
Provides:       compat-wxWidgets-gtk2-devel = %{version}-%{release}

%description -n compat-wxGTK3-gtk2-devel
This package contains all files needed for developing with GTK2-backed wxWidgets.


%package        i18n
Summary:        i18n message catalogs of the wxGTK3
BuildArch:      noarch

%description i18n
This package contains i18n message catalogs for the wxWidgets library.

%package -n     wxBase3
Summary:        Non-GUI support classes of the wxGTK3
Provides:       compat-wxBase3-gtk2 = %{version}-%{release}
Obsoletes:      compat-wxBase3-gtk2 < %{version}-%{release}

%description -n wxBase3
Non-GUI support classes from the wxWidgets library.

%package        help
Summary:        Help documentation for the wxGTK3 library
Requires:       %{name} = %{version}-%{release}
Provides:       wxWidgets-docs = %{version}-%{release} compat-wxWidgets-gtk2 = %{version}-%{release}
Provides:       wxWidgets-xmldocs = %{version}-%{release}
Provides:       wxGTK3-docs = %{version}-%{release} compat-wxGTK3-gtk2-docs = %{version}-%{release}
Provides:       wxGTK3-xmldocs = %{version}-%{release} compat-wxGTK3-gtk2-xmldocs = %{version}-%{release}
Obsoletes:      wxGTK3-docs < %{version}-%{release} compat-wxGTK3-gtk2-docs < %{version}-%{release}
Obsoletes:      wxGTK3-xmldocs < %{version}-%{release} compat-wxGTK3-gtk2-xmldocs < %{version}-%{release}
BuildArch:      noarch

%description    help
This package provides documentation for the wxGTK3-docs library.

%prep
%autosetup -n wxWidgets-%{version} -p1

sed -i -e 's|aclocal)|aclocal/wxwin32.m4)|;s|wxstd.mo|wxstd3.mo|;s|wxmsw.mo|wxmsw3.mo|' Makefile.in
sed -i -e 's|/usr/lib\b|%{_libdir}|' wx-config.in configure
sed -i -e 's|/lib|/%{_lib}|' src/unix/stdpaths.cpp
sed -i -e 's|$CPPUNIT_CONFIG --version|$CPPUNIT_CONFIG --modversion|' configure

%build
CFLAGS="$RPM_OPT_FLAGS -fno-strict-aliasing"
CXXFLAGS="$RPM_OPT_FLAGS -fno-strict-aliasing"
export LDFLAGS="%{build_ldflags} -Wl,-z,relro -Wl,-z,now -Wl,-z,noexecstack -Wl,--as-needed"
export CPPUNIT_CONFIG="/usr/bin/pkg-config cppunit"

%global _configure ../configure

install -d bld_gtk2
cd bld_gtk2
%configure --with-gtk=2 --with-opengl --with-sdl --with-libmspack --enable-intl \
  --disable-rpath --disable-glcanvasegl --enable-ipv6

%make_build
cd -

install -d bld_gtk3
cd bld_gtk3
%configure --with-gtk=3 --with-opengl --with-sdl --with-libmspack --enable-intl \
  --disable-rpath --disable-glcanvasegl --enable-ipv6

%make_build
cd -

WX_SKIP_DOXYGEN_VERSION_CHECK=1 docs/doxygen/regen.sh html
mv docs/doxygen/out/html .

cd docs/doxygen
WX_SKIP_DOXYGEN_VERSION_CHECK=1 ./regen.sh xml
cd -

%install
cd bld_gtk2
%make_install
cd -

cd bld_gtk3
%make_install
cd -

rm %{buildroot}%{_bindir}/wx-config
install -pD -m 755 %{S:1} %{buildroot}%{_libexecdir}/wxGTK3/wx-config
sed -i -e 's|=/usr|=%{_prefix}|' %{buildroot}%{_libexecdir}/%{name}/wx-config
ln -s ../..%{_libexecdir}/wxGTK3/wx-config %{buildroot}%{_bindir}/wx-config-3.2
touch %{buildroot}%{_bindir}/wx-config

mv %{buildroot}%{_bindir}/wxrc* %{buildroot}%{_libexecdir}/wxGTK3
ln -s ../..%{_libexecdir}/wxGTK3/wxrc-3.2 %{buildroot}%{_bindir}/wxrc-3.2
touch %{buildroot}%{_bindir}/wxrc


install -d %{buildroot}%{_datadir}/bakefile/presets/wx32
mv %{buildroot}%{_datadir}/bakefile/presets/*.* %{buildroot}%{_datadir}/bakefile/presets/wx32

%find_lang wxstd-3.2

%check
cd bld_gtk2/tests
%make_build test
cd -

cd bld_gtk3/tests
%make_build test
cd -

%post -n wxBase3-devel
if [ -f %{_bindir}/wx-config ] && [ ! -h %{_bindir}/wx-config ] ; then rm %{_bindir}/wx-config; fi;

%{_sbindir}/update-alternatives --install %{_bindir}/wx-config wx-config %{_libexecdir}/%{name}/wx-config 25

%{_sbindir}/update-alternatives --install %{_bindir}/wxrc wxrc %{_libexecdir}/%{name}/wxrc 25

%postun -n wxBase3-devel
if [ $1 -eq 0 ] ; then
  %{_sbindir}/update-alternatives --remove wx-config %{_libexecdir}/%{name}/wx-config
  %{_sbindir}/update-alternatives --remove wxrc %{_libexecdir}/%{name}/wxrc
fi

%files
%{_libdir}/libwx_gtk3u_*.so.*
%dir %{_libdir}/wx
%{_libdir}/wx/3.2

%files -n compat-wxGTK3-gtk2
%{_libdir}/libwx_gtk2u_*.so.*

%files -n wxBase3-devel
%ghost %{_bindir}/{wx-config,wxrc}
%{_bindir}/{wxrc-3.2,wx-config-3.2}
%{_includedir}/wx-3.2
%{_libdir}/libwx_baseu*.so
%dir %{_libdir}/wx
%dir %{_libdir}/wx/config
%dir %{_libdir}/wx/include
%{_datadir}/{aclocal/wxwin32.m4,bakefile/presets/wx32}
%{_libexecdir}/wxGTK3

%files devel
%{_libdir}/libwx_gtk3u_*.so
%{_libdir}/wx/{config/gtk3-unicode-3.2,include/gtk3-unicode-3.2}

%files -n compat-wxGTK3-gtk2-devel
%{_libdir}/libwx_gtk2u_*.so
%{_libdir}/wx/{config/gtk2-unicode-3.2,include/gtk2-unicode-3.2}

%files i18n -f wxstd-3.2.lang

%files -n wxBase3
%{_libdir}/libwx_baseu*.so.*

%files help
%doc docs/*
%doc html

%changelog
* Fri Mar 07 2025 mahailiang <mahailiang@uniontech.com> - 3.2.6-2
- adjust libdir at wx-config on sw_64

* Mon Sep 16 2024 Funda Wang <fundawang@yeah.net> - 3.2.6-1
- update to 3.2.6

* Tue Sep 26 2023 lvgenggeng <lvgenggeng@uniontech.com> - 3.2.2.1-2
- support sw_64

* Wed Sep 06 2023 wulei <wu_lei@hoperun.com> - 3.2.2.1-1
- Update to 3.2.2.1

* Wed Jun 07 2023 xu_ping <707078654@qq.com> - 3.0.4-9
- Add pie compile option

* Mon Nov 28 2022 huajingyun <huajingyun@loongson.cn> - 3.0.4-8
- modify wx-config to support loongarch64

* Wed Jul 27 2022 yaoxin <yaoxin30@h-partners.com> - 3.0.4-7
- License compliance rectification

* Mon May 31 2021 huanghaitao <huanghaitao8@huawei.com> - 3.0.4-6
- Completing build dependencies to fix gcc/gcc-c++ compiler missing error

* Fri Dec 06 2019 daiqianwen <daiqianwen@huawei.com> - 3.0.4-5
- Package init


